# Design Patterns

## Sub Topics
- [ ] What is a design pattern?
- [ ] "Design patterns" vs "Algorithms"
- [ ] What does the pattern consist of?
- [Return to Main Page](/Readme.md)

## Legend: Markers in Summary
- :ok_hand: - Correct answer
- :neutral_face: - Close enough answer
- :-1: - Incorrect answer

## What is a design pattern?
### Stats
- **Learn attempt count**: 7
- **Recall attempt count**: 6

<details>
<summary><em>Click me to collapse/fold.</em></summary>
	
### Details
**Design patterns** are typical solutions to commonly occurring problems in software design. 
They are like pre-made blueprints that you can customize to solve a recurring design problem in your code.

You can’t just find a pattern and copy it into your program, the way you can with off-the-shelf functions or libraries. The pattern is not a specific piece of code, but a general concept for solving a particular problem. You can follow the pattern details and implement a solution that suits the realities of your own program.

#### Summary and Recall Attempt
**Score**: 2/4

- Design patterns are typical solutions to common problems in software design.
    > :ok_hand: Design patterns are typical solutions to common ~~occuring~~ problems in software design
- They are like pre-made blueprints that you can customize to solve a recurring problem in your code.
    > :-1: Patterns are pre-made blueprints that ~~describe a general overview on how to solve a problem.~~
- A pattern is not a specific piece of code, but a general concept for solving a particular problem.
    > :ok_hand: A pattern is not a specific type of code, ~~rather it is~~ a general overview on how to solve a problem.
- You can follow the pattern details and implement a solution that suits the realities of your own program.
    > :-1: The same pattern applied to two different programs will have a different approach

</details>

## "Design patterns" vs "Algorithms"
### Stats
- **Learn attempt count**: 5
- **Recall attempt count**: 4

<details>
<summary><em>Click me to collapse/fold.</em></summary>

### Details
Patterns are often confused with algorithms, because both concepts describe typical solutions to some known problems. While an algorithm always defines a clear set of actions that can achieve some goal, a pattern is a more high-level description of a solution. The code of the same pattern applied to two different programs may be different.

An analogy to an algorithm is a cooking recipe: both have clear steps to achieve a goal. On the other hand, a pattern is more like a blueprint: you can see what the result and its features are, but the exact order of implementation is up to you.

#### Summary and Recall Attempt
**Score**: 3/5

- "Design patterns" and "Algorithms" are both concepts that describe typical solutions to some known problems.
    > :-1: Design patterns and algorithms ~~are both describe~~ solutions to ~~common problems in software design.~~
- An algorithm always defines a clear set of actions that can achieve some goal.
    > :ok_hand: An algorithm always ~~provides~~ clear set of actions to achieve ~~a goal~~.
- A design pattern is a high-level description of a solution. The code of the same pattern applied to two different programes may be different.
    > :neutral_face: A pattern is a high-level ~~decsription~~ of a solution. The same pattern applied to two different programs ~~will have a different result~~.
- An analogy to an algorithm is a cooking receipe, it has clear steps to achieve a goal.
    > :ok_hand: An analogy for an algorithm is a cooking recipe, it has ~~a clear set of~~ steps to achieve a goal.
- On the other hand, a pattern is more like a blueprint, you can see what the results and its features are, but the exact order of implementation is up to you.
    > :ok_hand: On the other hand, a pattern ~~is like~~ a blueprint, you can see ~~its results~~ and its features, but ~~the order~~ of implementation is up to you.

</details>

## What does the pattern consist of?
### Stats
- **Learn attempt count**: 0
- **Recall attempt count**: 0

<details>
<summary><em>Click me to collapse/fold.</em></summary>

### Details
- T.B.D

#### Summary and Recall Attempt
**Score**: 0/4

- T.B.D

</details>

## **TEMPLATE** Topic
### Stats
- **Learn attempt count**: 0
- **Recall attempt count**: 0

<details>
<summary><em>Click me to collapse/fold.</em></summary>

### Details
- T.B.D

#### Summary and Recall Attempt
**Score**: 0/4

- T.B.D

</details>
