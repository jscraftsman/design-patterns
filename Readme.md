# Learning Repository: Design Patterns

Learning repository. 

This project contains lessons learned from Design Pattern resources **(summary and link references below)**.

![Feynman Technique](/Learning Guides/learning-guide.png)

## Topics

1. [Design Patterns](/Design Patterns.md)

## Review Checklist
Current Item: [Design Patterns: What is a design pattern?](/Design Patterns.md#what-is-a-design-pattern)